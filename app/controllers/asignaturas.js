	var Asignatura = require('../models/asignatura');

	var list = async (req, res) => {
		await Asignatura.find(function (err, asignatura) {
			return asignatura;
		});
	}

	var crear = (req, res) => {
		let asignatura = new Asignatura({
			nombre : req.body.nombre,
			numHoras : req.body.horas, 
			docente : req.body.docente,
			alumnos : req.body.alumnos
		});
	}

	var deleteAsignatura = async (req,res) => { 
		await Asignatura.findByIdAndDelete(req.body.id);
	}

	var modificarAsignatura = async (req, res) => {
		await Asignatura.findByIdAndUpdate(req.body.id);
	}

	exports = {
		list,
		crear,
		deleteAsignatura,
		modificarAsignatura
	}