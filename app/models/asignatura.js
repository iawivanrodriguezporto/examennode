var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var asignaturaSchema = new Schema({
    nombre: String, 
	numHoras: String,
	docente: Number,
	alumnos: [Number]
});

module.exports = mongoose.model('asignatura', asignaturaSchema);