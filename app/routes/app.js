var express = require("express");
var path = require("path");
var router = express.Router();
const { list } = require('../controllers/asignaturas'); 

router.get("/", async (req, res, next) => {
    res.render("index.pug");
});

router.get('/asignatura', (req, res) => {
    res.render("newAsignatura.pug");
});


module.exports = router;